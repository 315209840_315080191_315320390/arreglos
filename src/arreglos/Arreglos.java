/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author 
 * Capelo Hernández Edgar Yair
 * Cornejo Hernández Maria Esther
 * Martínez Pérez Tania Guadalupe
 */
public class Arreglos {
    public static void main(String[] args) {
    
        double [] arrDouble= new double[10];
        for (int i=0; i<10; i++){
            arrDouble[i]=Aleatorio.n_A_M(0,100,3);
        }
        for(int i=0; i<arrDouble.length; i++){
            System.out.println("arrDouble["+i+"]="+arrDouble[i]);
        }
        
        int[][] arrDosDim = new int[5][8];
        for(int i=0; i<5;i++){
            System.out.print("\n");
            for(int j=0; j<8; j++){
                arrDosDim[i][j]=Aleatorio.n_A_M(0, 100);
                //System.out.print("arrDosDim{"+i+"]["+j+"]="+arrDosDim[i][j]+"\t");
                System.out.print(arrDosDim[i][j]+"\t");
            }
        }
        
    }
    
}

